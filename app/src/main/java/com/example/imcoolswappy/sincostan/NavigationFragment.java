package com.example.imcoolswappy.sincostan;

import android.app.ExpandableListActivity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by imcoolswappy on 2/17/2016.
 */
public class NavigationFragment extends Fragment {

    private ListView listView;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.nav_fragment,null);
       // listView = (ListView) view.findViewById(R.id.lv_list);
        String[] listData = getResources().getStringArray(R.array.mainFields);
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);

        prepareListData();


        listAdapter = new ExpandableAdaptor(getActivity(),listDataHeader,listDataChild);

        expListView.setAdapter(listAdapter);

        return view;
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        //Adding sub sections data
        String[] mainHeadings = getResources().getStringArray(R.array.mainFields);
        String[] childArr1 = getResources().getStringArray(R.array.algFormContent);
        String[] childArr2 = getResources().getStringArray(R.array.funcFormContent);
        String[] childArr3 = getResources().getStringArray(R.array.analyticGeomContent);
        String[] childArr4 = getResources().getStringArray(R.array.limAndDerivContent);
        String[] childArr5 = getResources().getStringArray(R.array.indefiniteIntContent);
        String[] childArr6 = getResources().getStringArray(R.array.definiteIntContent);
        String[] childArr7 = getResources().getStringArray(R.array.seriesContent);

        listDataHeader = Arrays.asList(mainHeadings);
        listDataChild.put(listDataHeader.get(0),Arrays.asList(childArr1));
        listDataChild.put(listDataHeader.get(1),Arrays.asList(childArr2));
        listDataChild.put(listDataHeader.get(2),Arrays.asList(childArr3));
        listDataChild.put(listDataHeader.get(3),Arrays.asList(childArr4));
        listDataChild.put(listDataHeader.get(4),Arrays.asList(childArr5));
        listDataChild.put(listDataHeader.get(5),Arrays.asList(childArr6));
        listDataChild.put(listDataHeader.get(6),Arrays.asList(childArr7));

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                /*if(listDataHeader.get(groupPosition).equals("Algebra Formulae")&&listDataChild.get(
                        listDataHeader.get(groupPosition)).get(
                        childPosition).equals("Ascending")) {

                }*/
                Toast.makeText(getActivity(),listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition)+" selected",Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }
}
